const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()
let port = process.env.PORT || 5000
app.use(cors())
app.use(bodyParser.json())

const { data } = require("./mocks/pickups")

app.get('/', (req, res) =>{
     return res.send("hola")
})

app.get('/api/user', (req, res) =>{
    res.status(201).json({
        "user":"user",
        "email":"user@example.com",
        "other":1
    })
})

app.post('/api/vtex', (req, res) =>{
    console.log("---------------------------------------")
    console.log(new Date())
    console.warn("REQUEST")
    console.log(req)
    console.warn("BODY")
    console.log(req.body)
    console.warn("HEADERS")
    console.log(req.headers)
    console.warn("PARAMS")
    console.log(req.params)
    console.log("---------------------------------------")
    res.status(201).json(req.body)
})

app.post('/api/vtex2', (req, res) =>{
    console.log("======================================")
    console.warn("BODY")
    console.log(req.body)
    console.warn("HEADERS")
    console.log(req.headers)
    let sum = 1 + 1
    try {
        console.log("======================================")
        res.status(200).json(null)
      } catch (err) {
        //... handle it locally
        throw new Error(err.message)
      }
    
})

// TIENDA NUBE PUNTOS
app.post('/api/tn', (req, res) =>{
    console.log("request ", req && req.body && req.body.destination && req.body.destination.postal_code)
    console.log("request header ",req.headers)
    console.log("request body ",req.body)
    const postalCode = req && req.body && req.body.destination && req.body.destination.postal_code
    const points = data.rates
    const filtered = postalCode ? points.filter(p => p.address.zipcode === postalCode) : points
    
    console.log("Response data ",{"rates": filtered.length ? filtered : points})
    
    res.status(200).json(
        {
            "rates": filtered.length ? filtered : points
        }
    )
})


// // app.post('/api/signup', addUser)


app.listen(port,()=>{
    console.log("server puerto ",port)
})
