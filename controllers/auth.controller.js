const { response } = require("express");
const { genereteJWT } = require("../helpers/genereteJWT");
const bcrypt = require("bcryptjs");
const { users } = require("../mocks/users")

const login = async (req, res = response) => {
  const { password, email } = req.body;

  try {
    const user = users.find(user => user.email === email)

    //const salt = bcrypt.genSaltSync(10)
    //user.password = bcrypt.hashSync(password, salt)

    if (!user) {
      return res.status(400).json({
        msg: "Usuario no encontrado",
      });
    }

    const validPassword = bcrypt.compareSync(password, user.password);
    if (!validPassword) {
      return res.status(400).json({
        msg: "Usuario no encontrado",
      });
    }

    const token = await genereteJWT(user.id.toString());

    res.json({
      user: {
        ...user,
        password: ''
      },
      token
    });
  } catch (error) {
    res.status(500).json({ msg: error });
  }
};

module.exports = {
  login
};
