const { response } = require('express');

const postVtexOne = async (req, res = response) => {
    console.log("======================================")
    console.log(new Date())
    console.warn("REQUEST")
    console.log(req)
    console.warn("BODY")
    console.log(req.body)
    console.warn("HEADERS")
    console.log(req.headers)
    console.warn("PARAMS")
    console.log(req.params)
    console.log("======================================")
    res.status(201).json(req.body)
}

const postVtexTwo = async (req, res = response) => {
    console.log("======================================")
    console.warn("BODY")
    console.log(req.body)
    console.warn("HEADERS")
    console.log(req.headers)

    res.status(200).json(null)
}

module.exports = {
    postVtexOne,
    postVtexTwo
}