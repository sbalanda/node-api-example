const { response } = require("express");
const { products } = require("../mocks/products");
const { data } = require("../mocks/pickups");

const allowedCollections = ["pickups", "products"];

const searchPickups = async (word = "", res, next) => {
    res.json({
        results: data,
      });
};

const searchProducts = async (word = "", res, next) => {
  let data = [];
  if (word == "all") {
    data = products;
  } else {
    data = products.filter(p=> p.id == word)
  }

  res.json({
    results: data,
  });
};

const search = (req, res = response) => {
  const { collection, word } = req.params;
  // console.log("req params ",req.params);
  // console.log("req query ",req.query);

  if (!allowedCollections.includes(collection)) {
    return res.status(400).json({
      msg: `Las colecciones permitidas son: ${allowedCollections}`,
    });
  }

  switch (collection) {
    case "pickups":
      searchPickups(word, res);
      break;
    case "products":
      searchProducts(word, res);
      break;

    default:
      res.status(500).json({
        msg: "Se le olvido hacer esta búsquda",
      });
  }
};

module.exports = {
  search,
};
