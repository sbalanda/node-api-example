const { response } = require("express");
const { products } = require("../mocks/products")

const get = async (req, res = response) => {
    res.json({products});
};

module.exports = {
  get
};