const { response } = require('express');
const { data } = require("../mocks/pickups")

const post = async (req, res = response) => {
    console.log("request ", req && req.body && req.body.destination && req.body.destination.postal_code)
    console.log("request header ",req.headers)
    console.log("request body ",req.body)
    const postalCode = req && req.body && req.body.destination && req.body.destination.postal_code
    const storeCode = req && req.body && req.body.destination && req.body.destination.storeCode
    const points = data.rates
    const filteredByCode = postalCode ? points.filter(p => p.address.zipcode === postalCode) : points
    const filtered = storeCode ? filteredByCode.filter(f => f.storeCode === storeCode) : filteredByCode

    if(!req.body.destination){
        res.status(400).json(
            {
                "codigoErrorInterno": 252,
                "mensaje": "El código postal enviado no es válido o no existe en Javit."
            }
        )
    }

    console.log("Response data ",{"rates": filtered.length ? filtered : points})
    
    res.status(200).json(
        {
            "rates": filtered.length ? filtered : points
        }
    )
}

module.exports = {
    post
}