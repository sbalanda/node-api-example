const cors = require("cors");
const express = require("express");
const fs = require('fs')

const swaggerUi = require('swagger-ui-express');

//TODO:: VER README
let swaggerDocument
if (process.env.NODE_ENV === 'development') {
    swaggerDocument = require('../swagger.json');
}else{
    swaggerDocument = require('../swagger_prod.json');
}
const customCss = fs.readFileSync((process.cwd()+"/swagger.css"), 'utf8');

class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT || 8081;

        // db

        // middleware
        this.middlewares()

        // rutes
        this.paths = {
            swagger: '/swagger',
            tn: '/api/tn',
            vtex: '/api/vtex',
            auth:'/api/auth',
            search:'/api/search',
        }     
        
        this.routes();
    }

    routes() {
        this.app.use(this.paths.swagger, swaggerUi.serve, swaggerUi.setup(swaggerDocument, {customCss} ))
        this.app.use(this.paths.tn, require('../routes/tn.route'))
        this.app.use(this.paths.vtex, require('../routes/vtex.route'))
        this.app.use(this.paths.auth, require('../routes/auth.route'))
        this.app.use(this.paths.search, require('../routes/search.route'))
    }

    middlewares() {
        this.app.use(cors())
        //parse body
        this.app.use(express.json())
        this.app.use(express.static('public'))
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log("PORT ", this.port);
        });
    }
}

module.exports = Server;
