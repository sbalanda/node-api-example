const { Router } = require("express")
const { check } = require("express-validator")
const { get } = require("../controllers/product.controller.js")

const router = Router()

router.get("/",get)

module.exports = router