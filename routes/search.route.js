const { Router } = require("express")
const { check } = require("express-validator")
const { search } = require("../controllers/search.controller.js")
const { validarJWT } = require("../middlewares/validate-jwt.js")

const router = Router()

router.get("/:collection/:word",[validarJWT],search)

module.exports = router