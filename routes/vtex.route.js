const { Router } = require("express")
const { check } = require("express-validator")
const { postVtexOne, postVtexTwo } = require("../controllers/vtex.controller")

const router = Router()

router.post("/",postVtexOne)

router.post("/EnviosVtex",postVtexTwo)

module.exports = router