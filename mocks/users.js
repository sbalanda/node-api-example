const users = [
    {
        "id": 1,
        "name": "Test",
        "username": "Test",
        "email": "test@email.com",
        "password": "$2a$10$NLHv8m8WMoQhbWfoedtgOu/vQTPVsgiWF4CIIjrvqAk3JtG4Nyl6y"
    }
]

module.exports = {
    users
}; 